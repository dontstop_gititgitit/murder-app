/*
  ID Number corresponds to the IUCR number which is the
  illinois uniform crime reporting.
  Leads to a more accurate way of filtering crimes, instead of just
  matching string names
  We can add more if necessary

*/
export const HOMICIDE = {
  first_degree: '"0110"',
  second_degree: '"0130"',
  involuntary_manslaughter: '"0141"',
  reckless_homicide: '"0142"'
}
export const SEXUAL_ASSAULT = {
  aggravated_with_hangun: '"0261"',
  aggravated_with_other_firearm: '"0262"',
  aggravated_with_knife: '"0263"',
  aggravated_with_other_dang_weapon: '"0264"',
  aggravated_with_other: '"265"'
}
export const ROBBERY = {
  armed_with_knife: '"0312"',
  armed_with_other_weapon: '"0313"',
  armed_with_gun: '"031A"',
  armed_with_other_firearm: '"031B"',
  aggravated_carjacking: '"0326"'
}
export const BATTERY = {
  aggravated_with_hangun: '"041A"',
  aggravated_with_other_firearm: '"041B"',
  aggravated_with_knife: '"0420"',
  aggravated_with_other_dang_weapon: '"0430"',
  aggravated_with_hand_fist_feet: '"0440"',
  aggravated_with_po_handgun: '"0450"',
  aggravated_with_po_other_firearm: '"0451"',
  aggravated_with_po_knife: '"0452"',
  aggravated_with_po_other_dang_weapon: '"0453"',
  aggravated_pro_hands: '"0454"',
  aggravated_with_hands_serious: '"0461"',
  aggravated_with_empty_hands_serious: '"0462"',
  aggravated_with_hands_serious: '"0479"',
  aggravated_pro_emp_handgun: '"0480"',
  aggravated_pro_emp_other_firearm: '"0481"',
  aggravated_pro_emp_kind: '"0482"',
  aggravated_pro_emp_other_dang_weapon: '"0483"'
}
export const RITUALISM = {
  agg_ritual_mut_handgun: '"0490"',
  agg_ritual_other_firearm: '"0491"',
  agg_ritual_knife: '"0492"',
  agg_ritual_other_dang_weapon: '"0493"',
  agg_ritual_other_hands_feet: '"0494"',
  agg_ritual_other_hands_feet_serious: '"0510"',
}
export const ASSAULT = {
  aggravated_handgun: '"051A"',
  aggravated_other_firearm: '"051B"',
  aggravated_knife: '"0520"',
  aggravated_other_dang_weapon: '"0530"',
  pro_emp_hands: '"0545"',
  agg_po_handgun: '"0550"',
  agg_po_other_firearm: '"0551"',
  agg_po_knife: '"0552"',
  agg_po_other_dang_weapon: '"0553"',
  agg_po_hands: '"0554"',
  agg_pro_emp_handgun: '"0555"',
  agg_pro_emp_other_firearm: '"0556"',
  agg_pro_emp_knife: '"0557"',
  agg_pro_emp_other_dang_weapon: '"0558"',
}
export const ARSON = {
  by_explosive: '"1010"',
  by_fire: '"1020"',
  aggravated: '"1025"',
  pos_explosive_incendiary_dev: '"1030"'
}
export const WEAPONS_VIOLATION = {
  unlawful_use_handgun: '"141A"',
  unlawful_use_other_firearm: '"141B"',
  unlawful_use_other_dang_wapon: '"141C"',
  reckless_firearm_discharge: '"1477"'
}
export const PUBLIC_PEACE_VIOLATION = {
  armed_violence: '"3200"'
}

export function buildQuery() {
      var filteredQuery = '';
// https://data.cityofchicago.org/resource/3uz7-d32j.json?$query=SELECT * WHERE _IUCR='0110' OR _IUCR='0312' OR _IUCR='041A' LIMIT 50000
  filteredQuery = 'SELECT * WHERE _IUCR= ' + ASSAULT.aggravated_handgun +
        ' OR _IUCR=' + ASSAULT.aggravated_other_firearm +
        ' OR _IUCR=' +  ASSAULT.aggravated_knife +
        ' OR _IUCR=' +  ASSAULT.aggravated_other_dang_weapon +
        ' OR _IUCR=' +  ASSAULT.pro_emp_hands +
        ' OR _IUCR=' +  ASSAULT.agg_po_handgun +
        ' OR _IUCR=' +  ASSAULT.agg_po_other_firearm +
        ' OR _IUCR=' +  ASSAULT.agg_po_other_dang_weapon +
        ' OR _IUCR=' +  ASSAULT.agg_po_hands +
        ' OR _IUCR=' +  ASSAULT.agg_pro_emp_handgun +
        ' OR _IUCR=' +  ASSAULT.agg_po_other_firearm +
        ' OR _IUCR=' +  ASSAULT.agg_pro_emp_knife +
        ' OR _IUCR=' +  ASSAULT.agg_pro_emp_other_dang_weapon +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_mut_handgun +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_other_firearm +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_knife +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_other_dang_weapon +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_other_hands_feet_serious +
        ' OR _IUCR=' +  RITUALISM.agg_ritual_other_hands_feet +
        ' OR _IUCR=' +  ARSON.by_explosive +
        ' OR _IUCR=' +  ARSON.by_fire +
        ' OR _IUCR=' +  ARSON.aggravated +
        ' OR _IUCR=' +  ARSON.pos_explosive_incendiary_dev +
        ' OR _IUCR=' +  WEAPONS_VIOLATION.unlawful_use_handgun +
        ' OR _IUCR=' +  WEAPONS_VIOLATION.unlawful_use_other_firearm +
        ' OR _IUCR=' +  WEAPONS_VIOLATION.unlawful_use_other_dang_wapon +
        ' OR _IUCR=' +  WEAPONS_VIOLATION.reckless_firearm_discharge +
        ' OR _IUCR=' +  PUBLIC_PEACE_VIOLATION.armed_violence +
        ' OR _IUCR=' +  BATTERY.aggravated_with_hangun +
        ' OR _IUCR=' +  BATTERY.aggravated_with_other_firearm +
        ' OR _IUCR=' +  BATTERY.aggravated_with_knife +
        ' OR _IUCR=' +  BATTERY.aggravated_with_other_dang_weapon +
        ' OR _IUCR=' +  BATTERY.aggravated_with_hand_fist_feet +
        ' OR _IUCR=' +  BATTERY.aggravated_with_po_handgun +
        ' OR _IUCR=' +  BATTERY.aggravated_with_po_other_firearm +
        ' OR _IUCR=' +  BATTERY.aggravated_with_po_knife +
        ' OR _IUCR=' +  BATTERY.aggravated_with_po_other_dang_weapon +
        ' OR _IUCR=' +  BATTERY.aggravated_with_empty_hands_serious +
        ' OR _IUCR=' +  BATTERY.aggravated_with_hands_serious +
        ' OR _IUCR=' +  BATTERY.aggravated_pro_hands +
        ' OR _IUCR=' +  HOMICIDE.first_degree +
        ' OR _IUCR=' +  HOMICIDE.second_degree +
        ' OR _IUCR=' +  HOMICIDE.involuntary_manslaughter +
        ' OR _IUCR=' +  HOMICIDE.reckless_homicide +
        ' OR _IUCR=' +  SEXUAL_ASSAULT.aggravated_with_hangun +
        ' OR _IUCR=' +  SEXUAL_ASSAULT.aggravated_with_other_firearm +
        ' OR _IUCR=' +  SEXUAL_ASSAULT.aggravated_with_knife +
        ' OR _IUCR=' +  SEXUAL_ASSAULT.aggravated_with_other_dang_weapon +
        ' OR _IUCR=' +  SEXUAL_ASSAULT.aggravated_with_other +
        ' OR _IUCR=' +  ROBBERY.armed_with_knife +
        ' OR _IUCR=' +  ROBBERY.armed_with_other_weapon +
        ' OR _IUCR=' +  ROBBERY.armed_with_gun +
        ' OR _IUCR=' +  ROBBERY.armed_with_other_firearm +
        ' LIMIT 50000'

  return filteredQuery;
}

export function filterByCrime(arr, ...crimes) {
  let filterArray = [];
  let newArray = [];
  crimes.forEach((value, index) => {
    newArray = arr.filter((crime) => {
      return crime._primary_decsription == value;
    })

    filterArray = filterArray.concat(newArray);
  });

  return filterArray;
}
