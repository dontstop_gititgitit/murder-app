import * as types from './constants';
import { buildQuery } from '../helpers/dataFilters';

export function getPosition() {
  return (dispatch, getState) => {

    if (!navigator.geolocation) {
      return dispatch({
        type: types.GET_POSITION,
        region: {
          coords: {
            latitude: 0,
            longitude: 0,
            error: 'you failed'
          },
          center: {
            latitude: 0,
            longitude: 0
          }
        }
      });
    }
    else {
      navigator.geolocation.getCurrentPosition((position) => {
        return dispatch({
          type: types.GET_POSITION,
          payload: {
            region: {
              coords: {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
              },
              center: {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
              }
            }
          }
        });
      });
    }
  }
}

export function watchPosition() {
  return (dispatch, getState) => {

    if (!navigator.geolocation) {
      return dispatch({
        type: types.WATCH_POSITION,
        region: {
          coords: {
            latitude: 0,
            longitude: 0,
            error: 'you failed'
          },
          center: {
            latitude: 0,
            longitude: 0
          }
        }
      });
    }
    else {
      navigator.geolocation.watchPosition((position) => {
        return dispatch({
          type: types.WATCH_POSITION,
          payload: {
            region: {
              coords: {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
              },
              center: {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
              }
            }
          }
        });
      });
    }
  }
}

export function getCrimeData() {
  return (dispatch, getState) => {
    let apiCall = fetch('https://data.cityofchicago.org/resource/3uz7-d32j.json?$query=' + buildQuery());
    apiCall.then((response) => response.json())
      .then((responseJson) => {
        // console.log(responseJson)
        return dispatch({
          type: types.GET_CRIME_DATA,
          payload: {
            crimes: responseJson
          }
        });
      })
      .catch((error) => {
        return dispatch({
          type: types.GET_CRIME_DATA,
          payload: {
            crimes: []
          }
        })
      });
  }
}

export function testRedux() {
  return {
    type: types.TEST_REDUX
  }
}
