import {
  Dimensions
} from 'react-native';

let width = Dimensions.get('window').width; //full width

export const mapStyles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    width: width,
    paddingTop: 20
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  header: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
  }
}
