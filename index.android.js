import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './reducers';
import MapContainer from './containers';
import HeaderComponent from './global_components/header'
/*__DEV__ Global node variable*/
const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

function configureStore(initialState) {
  const enhancer = compose(applyMiddleware(thunkMiddleware, loggerMiddleware));

  return createStore(reducer, initialState, enhancer);
}

const store = configureStore();

import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import {mapStyles} from './styles/map_styles.js';

export default class CrimeWatch extends Component {
  render() {
    return (
      <View style={styles.container}>
        <HeaderComponent />
        <Provider store={store}>
          <MapContainer></MapContainer>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create(mapStyles);

AppRegistry.registerComponent('CrimeWatch', () => CrimeWatch);
