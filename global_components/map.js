import React, { Component } from 'react'
import { AppRegistry, StyleSheet, View } from 'react-native'
import MapView from 'react-native-maps'
import { mapStyles } from '../styles/map_styles'
import { filterByCrime } from '../helpers/dataFilters'

import HeaderComponent from './header'
import LocationMarkerComponent from './location-marker'
import CurrentLocationMarkerComponent from './current-location-marker'

export default class MapComponent extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount () {
    this.props.getPosition();
    this.props.getCrimeData();
  }

  componentDidMount () {
    this.props.watchPosition();
  }

  componentDidUpdate() {
    if(this.props.crimes.crimes.length > 0) {
    //  console.log(filterByCrime(this.props.crimes.crimes, 'HOMICIDE', 'SEXUAL OFFENSE'));
    }
  }

  componentWillUnmount() {
    //navigator.geolocation.clearWatch(this.watchId)
  }

  render () {
    return (
      <View style={styles.container}>

        {this.props.region ? <MapView
          style={styles.map}
          initialRegion={this.props.region.coords}
          region={this.props.region.coords}
        >
        <LocationMarkerComponent center={this.props.region.center} crimes={this.props.crimes.crimes} />
        <CurrentLocationMarkerComponent region={this.props.region}/>
        </MapView> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create(mapStyles);

AppRegistry.registerComponent('MapComponent', () => MapComponent);
