import React, { Component } from 'react'
import { AppRegistry, View } from 'react-native'
import MapView from 'react-native-maps'
import _ from 'lodash'

export default class LocationMarker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      crimes: []
    }
  }

  componentDidUpdate() {
    
      // this.generateMarker();
  
  }
  generateMarker (distance =  0.00724500725) {
    let list = this.props.crimes;
    let newList = [];
    
    let maxLat = this.props.center.latitude + distance;
    let minLat = this.props.center.latitude - distance;
    let maxLon = this.props.center.longitude + distance;
    let minLon = this.props.center.longitude - distance;

    let long;
    let lat;

    list.forEach((key, index) => {
      long = !isNaN(parseFloat(key.longitude)) ? parseFloat(key.longitude) : 0;
      lat = !isNaN(parseFloat(key.latitude)) ? parseFloat(key.latitude) : 0;

      if ( (lat > minLat && lat < maxLat) && (long > minLon && long < maxLon) ){
        let newItem = {
          coordinates: {
            latitude:lat,
            longitude: long
          },
          type: key._primary_decsription,
          desc: key._secondary_description
        }

        newList.push(newItem)
      }
    })
    this.setState({crimes: newList})
  }

  render () {
    let i = 0;
      
    return (
      <View>
        {this.state.crimes.map(marker => (
          <MapView.Marker key={i++} coordinate={marker.coordinates} />
        ))}
      </View>
    );
  }
}

AppRegistry.registerComponent('LocationMarkerComponent', () => LocationMarkerComponent);
