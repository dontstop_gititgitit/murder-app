import React, { Component } from 'react'
import { AppRegistry, StyleSheet, View } from 'react-native'
import MapView from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome';

import MenuComponent from './menu'

import { mapStyles } from '../styles/map_styles'

export default class Header extends Component {
  render () {
    return (
      <View style={styles.header}>
        <Icon name="compass" size={30} color="#000" />
      </View>
    );
  }
}

const styles = StyleSheet.create(mapStyles);

AppRegistry.registerComponent('HeaderComponent', () => HeaderComponent);
