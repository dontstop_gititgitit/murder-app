import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Animated,
  Easing,
  Dimensions,
  TouchableHighlight,
  FlatList,
  Switch
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

var {height, width} = Dimensions.get('window')

export default class MenuComponent extends Component {
  constructor() {
    super()
    this.state = {
      expanded: false
    }
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(height - 50)
  }

  toggle() {
    (this.state.expanded == true) ? this.close() : this.open()
    this.setState({expanded: !this.state.expanded})
  }

  open() {
    Animated.timing(this.animatedValue, {
      toValue:  70,
      duration: 1000,
      easing: Easing.bounce
    }).start()
  }

  close() {
    Animated.timing(this.animatedValue, {
      toValue: height - 50,
      duration: 1000,
      easing: Easing.bounce
    }).start()
  }

  render() {
    const animatedStyle = { top: this.animatedValue }
    return (
      <Animated.View style={[ menuStyles.menu, animatedStyle ]}>
        <TouchableHighlight
          style={menuStyles.menuButton}
          underlayColor="lightgray"
          onPress={() => this.toggle()}>

          <Icon name="ellipsis-h" size={30} color="#000" />

        </TouchableHighlight>
        <View style={menuStyles.menuInner}>
          <FlatList
            data={[
              {
                filterName: 'Filter A',
                filterStatus: false
              },
              {
                filterName: 'Filter B',
                filterStatus: true
              },
              {
                filterName: 'Filter C',
                filterStatus: true
              },
              {
                filterName: 'Filter D',
                filterStatus: false
              },
            ]}
            renderItem={({item}) => <View style={menuStyles.item}><Text style={menuStyles.itemText}>{item.filterName}</Text><Switch style={menuStyles.itemSwitch} value={item.filterStatus}/></View>}
          />
        </View>
      </Animated.View>
    );
  }
}

const menuStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  screenContent: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  map: {
    position: 'absolute',
    width: width,
    height: height,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  menu: {
    flex: 1,
    position: 'absolute',
    width: width,
    top: height - 50,
    backgroundColor: 'lightgray'
  },
  menuButton: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  menuInner: {
    alignItems: 'center',
    justifyContent: 'center',
    height: height - 50,
    backgroundColor: '#999',
    paddingTop: 30
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  itemText: {
    left: 50,
    alignItems: 'center',
    color: 'white',
    fontSize: 16,
    paddingTop: 6
  },
  itemSwitch: {
    right: 50,
    alignItems: 'center'
  }
});

AppRegistry.registerComponent('MenuComponent', () => MenuComponent);
