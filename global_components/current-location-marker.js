import React, { Component } from 'react'
import { AppRegistry, View } from 'react-native'
import MapView from 'react-native-maps'

export default class CurrentLocationMarker extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
  // this.getPosition();
    console.log(this.props)
  }

  componentDidMount() {
  //  this.watchPosition();
  }

  render () {
    return (
      <View>
        {this.props.region.center ? <MapView.Marker
          coordinate={this.props.region.center}
        /> : null }

        {this.props.region.coords ? <MapView.Circle
          key={this.props.region.coords.latitude + this.props.region.coords.longitude}
          center={this.props.region.center}
          radius={805}
          strokeColor="#999"
          fillColor="rgba(0, 0, 0, 0.1)"
        /> : null }
      </View>
    );
  }
}

AppRegistry.registerComponent('CurrentLocationMarkerComponent', () => CurrentLocationMarkerComponent);
