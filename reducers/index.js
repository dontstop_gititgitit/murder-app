import * as types from '../actions/constants'
import { combineReducers } from 'redux';

const getPosition = (state = {
  coords: {
    longitude: 0.0,
    latitude: 0.0,
    latitudeDelta: 0.0,
    longitudeDelta: 0.0
  },
  center: {
    latitude: 0,
    longitude: 0
  }
}, action) => {
  switch (action.type) {
    case types.GET_POSITION:
      return state = {
        coords: {
          latitude: action.payload.region.coords.latitude,
          longitude: action.payload.region.coords.longitude,
          latitudeDelta: 0.02,
          longitudeDelta: 0.02
        },
        center: {
          latitude: action.payload.region.coords.latitude,
          longitude: action.payload.region.coords.longitude,
        }
      }
    case types.WATCH_POSITION:
      return state = {
        coords: {
          latitude: action.payload.region.coords.latitude,
          longitude: action.payload.region.coords.longitude,
          latitudeDelta: 0.02,
          longitudeDelta: 0.02
        },
        center: {
          latitude: action.payload.region.coords.latitude,
          longitude: action.payload.region.coords.longitude,
        }
      }
    default:
      return state
  }
}

const getCrimeData = (state = {
  crimes: []
}, action) => {
  switch(action.type) {
    case types.GET_CRIME_DATA:
      return state = {
        crimes: action.payload.crimes
      }
    default:
      return state
  }
}

const reducers = combineReducers({
  getPosition,
  getCrimeData
});

export default reducers;
