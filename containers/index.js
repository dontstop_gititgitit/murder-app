import React, { Component} from 'react';
import MapComponent from '../global_components/map.js';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import { bindActionCreators } from 'redux';

class MapContainer extends Component {
  render() {
    return (
      <View>
        <MapComponent {...this.props} />
      </View>
    )
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    region: state.getPosition,
    crimes: state.getCrimeData,
    testRedux: state.testRedux
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapComponent);
