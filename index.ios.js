import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './reducers';
import MapContainer from './containers';
import HeaderComponent from './global_components/header'
import MenuComponent from './global_components/menu'
import { mapStyles } from './styles/map_styles.js';

/*__DEV__ Global node variable*/
const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

function configureStore(initialState) {
  const enhancer = compose(applyMiddleware(thunkMiddleware, loggerMiddleware));

  return createStore(reducer, initialState, enhancer);
}

const store = configureStore();

// get those warnings outta here
console.disableYellowBox = true;

export default class CrimeWatch extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <HeaderComponent />
          <MapContainer></MapContainer>
          <MenuComponent />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create(mapStyles);

AppRegistry.registerComponent('CrimeWatch', () => CrimeWatch);
